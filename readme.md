# using this repository

You'll need to create an inventory file (`inventory.ini`) containing the details of how to access the server you want to deploy to. Check the ansible documentation for details.

Then just run `ansible-playbook -i inventory.ini site.yml`
